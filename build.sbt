name := "sqds"

description := "skunk, quill, doobie, slick"

libraryDependencies ++= {
  lazy val catsVersion = "3.4.8"
  lazy val circeVersion = "0.14.1"

  Seq(
    "org.typelevel"         %% "cats-effect"            % catsVersion,
    "io.circe"              %% "circe-core"             % circeVersion,
    "io.circe"              %% "circe-generic"          % circeVersion,
    "io.circe"              %% "circe-config"           % "0.8.0",

    "org.tpolecat"          %% "skunk-core"             % "0.5.1",

    "org.tpolecat"          %% "doobie-core"            % "1.0.0-RC1",
    "org.tpolecat"          %% "doobie-hikari"          % "1.0.0-RC1",
    "org.tpolecat"          %% "doobie-postgres"        % "1.0.0-RC1",

    "io.getquill"           %% "quill-async-postgres"   % "3.12.0",
    "org.postgresql"        % "postgresql"              % "42.5.4",

    "com.typesafe.slick"    %% "slick"                  % "3.4.1",
    "com.typesafe.slick"    %% "slick-hikaricp"         % "3.4.1",
    "com.github.tminglei"   %% "slick-pg"               % "0.21.1",

    "ch.qos.logback"        % "logback-classic"         % "1.4.5"                 % Runtime
  )
}
