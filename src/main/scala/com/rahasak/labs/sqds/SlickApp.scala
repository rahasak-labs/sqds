package com.rahasak.labs.sqds

import cats.effect.{ExitCode, IO, IOApp}
import com.rahasak.labs.sqds.config.Config
import com.rahasak.labs.sqds.conn.ConnectionPoolSlick
import com.rahasak.labs.sqds.model.Toggle
import com.rahasak.labs.sqds.repo.live.ToggleRepoImplSlick
import com.typesafe.scalalogging.LazyLogging

object SlickApp extends IOApp with LazyLogging {
  override def run(args: List[String]): IO[ExitCode] = {
    val program: IO[Unit] = for {
      config <- Config.load()
      connectionPool <- IO(ConnectionPoolSlick.pool(config.dbConfig))
      repo = new ToggleRepoImplSlick(connectionPool)
      _ <- repo.init()
      toggle = Toggle(service = "slick", name = "rahasak", value = "kooooo")
      _ <- repo.createToggle(toggle)
      //search <- repo.getToggles("slick")
      _ <- repo.updateToggle("rahasak", "ops")
      search <- repo.getToggle("rahasak")
      _ <- IO(logger.info(s"toggles $search"))
      _ <- repo.deleteToggle("rahasak")
    } yield {
      ()
    }
    program.as(ExitCode.Success)
  }
}
