package com.rahasak.labs.sqds

import cats.effect.{ExitCode, IO, IOApp}
import com.rahasak.labs.sqds.config.Config
import com.rahasak.labs.sqds.conn.ConnectionPoolQuill
import com.rahasak.labs.sqds.model.Toggle
import com.rahasak.labs.sqds.repo.live.ToggleRepoLiveQuill
import com.typesafe.scalalogging.LazyLogging

object QuillApp extends IOApp with LazyLogging {
  override def run(args: List[String]): IO[ExitCode] = {
    val program: IO[Unit] = for {
      config <- Config.load()
      connectionPool <- IO(ConnectionPoolQuill.pool(config.dbConfig))
      repo = new ToggleRepoLiveQuill(connectionPool)
      //_ <- repo.init()
      toggle = Toggle(service = "quill", name = "rahasak", value = "labs")
      _ <- repo.createToggle(toggle)
      //search <- repo.getServiceToggles("quill")
      search <- repo.getAllToggles
      _ <- IO(logger.info(s"toggles $search"))
      _ <- repo.deleteToggle("rahasak")
    } yield {
      ()
    }
    program.as(ExitCode.Success)
  }
}
