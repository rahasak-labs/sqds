package com.rahasak.labs.sqds

import cats.effect.{ExitCode, IO, IOApp}
import com.rahasak.labs.sqds.config.Config
import com.rahasak.labs.sqds.conn.ConnectionPoolDoobie
import com.rahasak.labs.sqds.model.Toggle
import com.rahasak.labs.sqds.repo.live.ToggleExecImplDoobie
import com.typesafe.scalalogging.LazyLogging

object DoobieApp extends IOApp with LazyLogging {
  override def run(args: List[String]): IO[ExitCode] = {
    val program: IO[Unit] = for {
      config <- Config.load()
      connectionPool <- IO(ConnectionPoolDoobie.pool(config.dbConfig))
      repo = new ToggleExecImplDoobie(connectionPool)
      _ <- repo.init()
      toggle = Toggle(service = "doobie", name = "rahasak", value = "labs")
      _ <- repo.createToggle(toggle)
      search <- repo.getAllToggles
      _ <- IO(logger.info(s"toggles $search"))
      //_ <- repo.updateToggle("rahasak", "ops")
      _ <- repo.deleteToggle("rahasak")
    } yield {
      ()
    }
    program.as(ExitCode.Success)
  }
}

