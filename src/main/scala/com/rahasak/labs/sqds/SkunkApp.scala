package com.rahasak.labs.sqds

import cats.effect.{ExitCode, IO, IOApp}
import com.rahasak.labs.sqds.config.Config
import com.rahasak.labs.sqds.conn.ConnectionPoolSkunk
import com.rahasak.labs.sqds.model.Toggle
import com.rahasak.labs.sqds.repo.live.ToggleRepoLiveSkunk
import com.typesafe.scalalogging.LazyLogging

object SkunkApp extends IOApp with LazyLogging {
  override def run(args: List[String]): IO[ExitCode] = {
    val program: IO[Unit] = for {
      config <- Config.load()
      connectionPool <- IO(ConnectionPoolSkunk.pool(config.dbConfig))
      repo = new ToggleRepoLiveSkunk(connectionPool)
      _ <- repo.init()
      toggle = Toggle(service = "skunk1", name = "lambda", value = "labs")
      _ <- repo.createToggle(toggle)
      //_ <- repo.updateToggle("rahasak", "ops")
      search <- repo.getServiceToggles("skunk1")
      //search <- repo.getAllToggles
      _ <- IO(logger.info(s"toggles $search"))
      _ <- repo.deleteToggle("lambda")
    } yield {
      ()
    }

    program.as(ExitCode.Success)
  }
}
