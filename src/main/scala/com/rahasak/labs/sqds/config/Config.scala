package com.rahasak.labs.sqds.config

import cats.effect.IO
import io.circe.config.parser
import io.circe.generic.auto._

case class HttpConfig(port: Int, host: String)

case class DbConfig(host: String, port: Int, database: String, username: String, password: String, poolSize: Int)

case class Config(httpConfig: HttpConfig, dbConfig: DbConfig)

object Config {
  def load(): IO[Config] = {
    for {
      httpConf <- parser.decodePathF[IO, HttpConfig]("http")
      dbConf <- parser.decodePathF[IO, DbConfig]("db")
    } yield Config(httpConf, dbConf)
  }
}
