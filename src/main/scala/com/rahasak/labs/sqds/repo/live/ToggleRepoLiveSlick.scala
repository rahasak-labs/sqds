package com.rahasak.labs.sqds.repo.live

import cats.effect.IO
import cats.effect.kernel.Resource
import com.rahasak.labs.sqds.model.Toggle
import com.rahasak.labs.sqds.repo.ToggleRepo
import slick.ast.BaseTypedType
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.{JdbcType, PostgresProfile}

import java.sql.Timestamp
import java.util.{Date, UUID}


/**
  * Define toggles table mapping with slick
  * @param tag tag
  */
class Toggles(tag: Tag) extends Table[Toggle](tag, "toggles") {
  implicit val dateColumnType: JdbcType[Date] with BaseTypedType[Date] = MappedColumnType.base[Date, Timestamp](
    d => new Timestamp(d.getTime),
    d => new Date(d.getTime)
  )

  def id = column[UUID]("id", O.PrimaryKey)

  def service = column[String]("service")

  def name = column[String]("name")

  def value = column[String]("value")

  def timestamp = column[Date]("timestamp")

  def * = (id.?, service, name, value, timestamp).<>(Toggle.tupled, Toggle.unapply)
}

class ToggleRepoImplSlick(connectionPool: Resource[IO, PostgresProfile.backend.DatabaseDef]) extends ToggleRepo {
  val toggles = TableQuery[Toggles]

  def init(): IO[Unit] = {
    connectionPool.use { session =>
      cats.effect.IO.fromFuture(cats.effect.IO(session.run(toggles.schema.createIfNotExists)))
    }
  }

  def createToggle(toggle: Toggle): IO[Unit] = {
    connectionPool.use { session =>
      cats.effect.IO.fromFuture(cats.effect.IO(session.run(toggles += toggle))).map(_ => ())
    }
  }

  def updateToggle(name: String, value: String): IO[Unit] = {
    connectionPool.use { session =>
      val q = for {
        t <- toggles
        if t.name === name
      } yield t.value
      cats.effect.IO.fromFuture(cats.effect.IO(session.run(q.update(value)))).map(_ => ())
    }
  }

  def getToggle(name: String): IO[Option[Toggle]] = {
    connectionPool.use { session =>
      cats.effect.IO.fromFuture(cats.effect.IO(session.run(toggles.filter(_.name === name).result.headOption)))
    }
  }

  def getServiceToggles(service: String): IO[List[Toggle]] = {
    connectionPool.use { session =>
      cats.effect.IO.fromFuture(cats.effect.IO(session.run(toggles.filter(_.service === service).result))).map(_.toList)
    }
  }

  def getAllToggles: IO[List[Toggle]] = {
    connectionPool.use { session =>
      cats.effect.IO.fromFuture(cats.effect.IO(session.run(toggles.result))).map(_.toList)
    }
  }

  def deleteToggle(name: String): IO[Unit] = {
    connectionPool.use { session =>
      val q = for {
        t <- toggles
        if t.name === name
      } yield t
      cats.effect.IO.fromFuture(cats.effect.IO(session.run(q.delete))).map(_ => ())
    }
  }
}


