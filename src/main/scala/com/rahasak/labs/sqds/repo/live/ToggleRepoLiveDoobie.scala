package com.rahasak.labs.sqds.repo.live

import cats.effect.kernel.Resource
import cats.effect.IO
import com.rahasak.labs.sqds.model.Toggle
import com.rahasak.labs.sqds.repo.ToggleRepo
import com.typesafe.scalalogging.LazyLogging
import doobie.hikari._
import doobie.implicits._
import doobie.postgres.implicits._

/**
  * Define doobie queries
  *   - table create
  *   - insert
  *   - update
  *   - select
  *   - delete
  */
object DoobieQueries {

  def tableQuery: doobie.Update0 = {
    sql"""
         |CREATE TABLE IF NOT EXISTS toggles (
         |  id UUID PRIMARY KEY,
         |  name VARCHAR(100),
         |  service TEXT,
         |  value TEXT,
         |  timestamp TIMESTAMP
         |)
       """.stripMargin
      .update
  }

  def insertQuery(toggle: Toggle): doobie.Update0 = {
    sql"""
         |INSERT INTO toggles (
         |  id,
         |  service,
         |  name,
         |  value,
         |  timestamp
         |)
         |VALUES (
         |  ${toggle.id},
         |  ${toggle.service},
         |  ${toggle.name},
         |  ${toggle.value},
         |  ${toggle.timestamp}
         |)
        """.stripMargin
      .update
  }

  def updateQuery(name: String, value: String): doobie.Update0 = {
    sql"""
         |UPDATE toggles
         |SET value = $value
         |WHERE name = $name
       """.stripMargin
      .update
  }

  def selectByNameQuery(name: String): doobie.Query0[Toggle] = {
    sql"""
         |SELECT id, service, name, value, timestamp
         |FROM toggles
         |WHERE name = $name
         |LIMIT 1
       """.stripMargin
      .query[Toggle]
  }

  def selectByServiceQuery(service: String): doobie.Query0[Toggle] = {
    sql"""
         |SELECT * FROM toggles
         |WHERE service = $service
       """.stripMargin
      .query[Toggle]
  }

  def selectAllQuery: doobie.Query0[Toggle] = {
    sql"""
         |SELECT * FROM toggles
       """.stripMargin
      .query[Toggle]
  }

  def deleteQuery(name: String): doobie.Update0 = {
    sql"""
         |DELETE FROM toggles
         |WHERE name=$name
       """.stripMargin
      .update
  }
}

class ToggleExecImplDoobie(connectionPool: Resource[IO, HikariTransactor[IO]]) extends ToggleRepo with LazyLogging {
  def init(): IO[Int] = {
    connectionPool.use { session =>
      DoobieQueries.tableQuery.run.transact(session)
    }
  }

  def createToggle(toggle: Toggle): IO[Unit] = {
    connectionPool.use { session =>
      DoobieQueries.insertQuery(toggle).run.transact(session).map(_ => ())
    }
  }

  def updateToggle(name: String, value: String): IO[Unit] = {
    connectionPool.use { session =>
      DoobieQueries.updateQuery(name, value).run.transact(session).map(_ => ())
    }
  }

  def getToggle(name: String): IO[Option[Toggle]] = {
    connectionPool.use { session =>
      DoobieQueries.selectByNameQuery(name).option.transact(session)
    }
  }

  def getServiceToggles(service: String): IO[List[Toggle]] = {
    connectionPool.use { session =>
      DoobieQueries.selectByServiceQuery(service).to[List].transact(session)
    }
  }

  def getAllToggles: IO[List[Toggle]] = {
    connectionPool.use { session =>
      DoobieQueries.selectAllQuery.to[List].transact(session)
    }
  }

  def deleteToggle(name: String): IO[Unit] = {
    connectionPool.use { session =>
      DoobieQueries.deleteQuery(name).run.transact(session).map(_ => ())
    }
  }

}

