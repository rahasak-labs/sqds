package com.rahasak.labs.sqds.repo.live

import cats.effect.{IO, Resource}
import com.rahasak.labs.sqds.model.Toggle
import com.rahasak.labs.sqds.repo.ToggleRepo
import io.getquill.{PostgresAsyncContext, SnakeCase}

class ToggleRepoLiveQuill(connectionPool: Resource[IO, PostgresAsyncContext[SnakeCase.type]]) extends ToggleRepo {
  implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

  //  def init(): IO[Unit] = {
  //    connectionPool.use { ctx: PostgresAsyncContext[SnakeCase.type] =>
  //      import ctx._
  //
  //      val q =
  //        """
  //          |CREATE TABLE IF NOT EXISTS toggles(
  //          |  id UUID PRIMARY KEY,
  //          |  name VARCHAR(100),
  //          |  service TEXT,
  //          |  value TEXT,
  //          |  timestamp TIMESTAMP
  //          |);
  //          |""".stripMargin
  //      val rawQuery = quote(infix"$q".as[Action[Any]])
  //      cats.effect.IO.fromFuture(cats.effect.IO(ctx.run(rawQuery))).map(_ => ())
  //    }
  //  }

  def createToggle(toggle: Toggle): IO[Unit] = {
    connectionPool.use { ctx =>
      import ctx._
      val q = quote {
        querySchema[Toggle]("toggles")
          .insert(lift(toggle))
      }
      cats.effect.IO.fromFuture(cats.effect.IO(ctx.run(q))).map(_ => ())
    }
  }

  def updateToggle(name: String, value: String): IO[Unit] = {
    connectionPool.use { ctx =>
      import ctx._
      val q = quote {
        querySchema[Toggle]("toggles")
          .filter(t => t.name == lift(name))
          .update(_.value -> lift(value))
      }
      cats.effect.IO.fromFuture(cats.effect.IO(ctx.run(q))).map(_ => ())
    }
  }

  def getToggle(name: String): IO[Option[Toggle]] = {
    connectionPool.use { ctx: PostgresAsyncContext[SnakeCase.type] =>
      import ctx._
      val a = quote {
        querySchema[Toggle]("toggles")
          .filter(t => t.name == lift(name))
      }
      cats.effect.IO.fromFuture(cats.effect.IO(ctx.run(a).map(_.headOption)))
    }
  }

  def getServiceToggles(service: String): IO[List[Toggle]] = {
    connectionPool.use { ctx: PostgresAsyncContext[SnakeCase.type] =>
      import ctx._
      val a = quote {
        querySchema[Toggle]("toggles")
          .filter(t => t.service == lift(service))
      }
      cats.effect.IO.fromFuture(cats.effect.IO(ctx.run(a).map(_.asInstanceOf[List[Toggle]])))
    }
  }

  def getAllToggles: IO[List[Toggle]] = {
    connectionPool.use { ctx: PostgresAsyncContext[SnakeCase.type] =>
      import ctx._
      val a = quote {
        querySchema[Toggle]("toggles")
      }
      cats.effect.IO.fromFuture(cats.effect.IO(ctx.run(a).map(_.asInstanceOf[List[Toggle]])))
    }
  }

  def deleteToggle(name: String): IO[Unit] = {
    connectionPool.use { ctx =>
      import ctx._
      val q = quote {
        querySchema[Toggle]("toggles")
          .filter(t => t.name == lift(name))
          .delete
      }
      cats.effect.IO.fromFuture(cats.effect.IO(ctx.run(q))).map(_ => ())
    }
  }
}


