package com.rahasak.labs.sqds.conn

import cats.effect.{IO, Resource}
import com.rahasak.labs.sqds.config.DbConfig
import com.typesafe.config.{ConfigFactory, ConfigValueFactory}
import io.getquill.{PostgresAsyncContext, SnakeCase}

object ConnectionPoolQuill {
  def pool(dbConfig: DbConfig): Resource[IO, PostgresAsyncContext[SnakeCase.type]] = {
    lazy val url = s"postgresql://${dbConfig.host}:${dbConfig.port}/${dbConfig.database}?user=${dbConfig.username}&password=${dbConfig.password}"
    val config = ConfigFactory.empty()
      .withValue("url", ConfigValueFactory.fromAnyRef(url))

    Resource.make(IO(new PostgresAsyncContext(SnakeCase, config)))(db => IO.unit)
  }
}
