package com.rahasak.labs.sqds.conn

import cats.effect.IO
import cats.effect.kernel.Resource
import com.rahasak.labs.sqds.config.DbConfig
import doobie._
import doobie.hikari._

object ConnectionPoolDoobie {

  def pool(dbConfig: DbConfig): Resource[IO, HikariTransactor[IO]] = for {
    ce <- ExecutionContexts.fixedThreadPool[IO](32) // our connect EC
    xa <- HikariTransactor.newHikariTransactor[IO](
      "org.postgresql.Driver",
      s"jdbc:postgresql://${dbConfig.host}:${dbConfig.port}/${dbConfig.database}",
      dbConfig.username,
      dbConfig.password,
      ce
    )
  } yield xa

}
