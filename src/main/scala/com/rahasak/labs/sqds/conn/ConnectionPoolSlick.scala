package com.rahasak.labs.sqds.conn

import cats.effect.IO
import cats.effect.kernel.Resource
import com.rahasak.labs.sqds.config.DbConfig
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import slick.jdbc.PostgresProfile
import slick.jdbc.PostgresProfile.api._

object ConnectionPoolSlick {
  def pool(dbConfig: DbConfig): Resource[IO, PostgresProfile.backend.DatabaseDef] = {
    lazy val url = s"jdbc:postgresql://${dbConfig.host}:${dbConfig.port}/${dbConfig.database}"
    lazy val driver = "org.postgresql.Driver"

    val hikariConfig = new HikariConfig
    hikariConfig.setDriverClassName(driver)
    hikariConfig.setUsername(dbConfig.username)
    hikariConfig.setPassword(dbConfig.password)
    hikariConfig.setJdbcUrl(url)
    hikariConfig.setConnectionTestQuery("SELECT 1")

    val ds = new HikariDataSource(hikariConfig)
    Resource.make(IO(Database.forDataSource(ds, Option(dbConfig.poolSize))))(db => IO.unit)
  }
}
