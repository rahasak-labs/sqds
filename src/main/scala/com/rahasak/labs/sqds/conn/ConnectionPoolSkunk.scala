package com.rahasak.labs.sqds.conn

import cats.effect.IO
import cats.effect.kernel.Resource
import com.rahasak.labs.sqds.config.DbConfig
import natchez.Trace.Implicits.noop
import skunk.Session

object ConnectionPoolSkunk {

  def pool(dbConfig: DbConfig): Resource[IO, Resource[IO, Session[IO]]] = {
    Session.pooled[IO](
      host = dbConfig.host,
      port = dbConfig.port,
      user = dbConfig.username,
      database = dbConfig.database,
      password = Option(dbConfig.password),
      max = dbConfig.poolSize
    )
  }

}
